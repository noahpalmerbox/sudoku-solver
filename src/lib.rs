//!Basic sudoku solver


use std::{
    convert::TryInto,
    fmt::{Display, Write},
    fs::File,
    io::{BufRead, BufReader},
    path::Path,
};

use thiserror::Error;





#[derive(Clone, Copy, PartialEq, Eq)]
pub enum SudokuDigit {
    ONE,
    TWO,
    THREE,
    FOUR,
    FIVE,
    SIX,
    SEVEN,
    EIGHT,
    NINE,
}


///Returns all the empty squares of a sudoku puzzle.
pub struct SudokuEmptySquareIterator<'a> {
    puzzle: &'a Sudoku,
    x: usize,
    y: usize,
}

impl Iterator for SudokuEmptySquareIterator<'_> {
    type Item = Index;

    fn next(&mut self) -> Option<Self::Item> {
        loop {
            if self.x > 8 {
                self.y += 1;
                self.x = 0;
            }
            if self.y > 8 {
                return None;
            }
            if self.puzzle.data[self.x + self.y * 9].is_none() {
                self.x += 1;
                return Some(Index::new(self.x - 1, self.y));
               
            } else {
                self.x += 1;
            }
        }
    }
}

#[derive(Error, Debug)]
pub enum SudokuError {
    #[error("This puzzle is unsolvable")]
    Unsolvable,
}

#[derive(Clone, Copy, PartialEq, Eq)]
pub struct Index {
    x: usize,
    y: usize,
}

impl Index {
    pub const fn new(x: usize, y: usize) -> Self {
        Index { x, y }
    }
    pub const fn get_x(&self) -> usize {
        self.x
    }
    pub const fn get_y(&self) -> usize {
        self.y
    }
    const fn get_data_index(&self) -> usize {
        self.x + self.y * 9
    }
}

///A Sudoku puzzle.
///
///Can be read from [read_file()] and solved with [solve()] 
pub struct Sudoku {
    data: [Option<SudokuDigit>; 81],
}

impl Sudoku {
    ///Gets all digits on the same row as an index.
    pub fn get_row(&self, index: Index) -> [Option<SudokuDigit>; 9] {
        self.data[index.get_y() * 9..(index.get_y() + 1) * 9]
            .try_into()
            .unwrap()
    }
    ///Gets all digits in the same column as an index
    pub fn get_column(&self, index: Index) -> [Option<SudokuDigit>; 9] {
        let mut col: [Option<SudokuDigit>; 9] = [None; 9];
        for i in 0..9 {
            col[i] = self.data[index.get_x() + (i * 9)];
        }
        col
    }
    ///Gets all digits in the same square as an index
    pub fn get_square(&self, index: Index) -> [Option<SudokuDigit>; 9] {
        let mut square: [Option<SudokuDigit>; 9] = [None; 9];
        let box_origin_x = (index.get_x() / 3) * 3;
        let box_origin_y = (index.get_y() / 3) * 3;
        for y in 0..3 {
            for x in 0..3 {
                square[x + y * 3] = self.data[(box_origin_x + x) + (box_origin_y + y) * 9]
            }
        }
        square
    }
    ///Returns all possible digits that a given index can have.
    ///
    ///If there are no valid digits returns an empty Vec.
    ///Doesn't make sense to call with a non-empty square
    pub fn get_valid_digits(&self, index: Index) -> Vec<SudokuDigit> {
        //Create a vec with all digits
        let mut digits = vec![
            SudokuDigit::ONE,
            SudokuDigit::TWO,
            SudokuDigit::THREE,
            SudokuDigit::FOUR,
            SudokuDigit::FIVE,
            SudokuDigit::SIX,
            SudokuDigit::SEVEN,
            SudokuDigit::EIGHT,
            SudokuDigit::NINE,
        ];

        //Remove all digits on the same column.
        for potential_digit in self.get_column(index) {
            if let Some(digit) = potential_digit {
                if let Some(pos) = digits.iter().position(|val| *val == digit) {
                    digits.remove(pos);
                }
            }
        }
        //Remove all digits on the same row.
        for potential_digit in self.get_row(index) {
            if let Some(digit) = potential_digit {
                if let Some(pos) = digits.iter().position(|val| *val == digit) {
                    digits.remove(pos);
                }
            }
        }
        //remove all digits in the same square
        for potential_digit in self.get_square(index) {
            if let Some(digit) = potential_digit {
                if let Some(pos) = digits.iter().position(|val| *val == digit) {
                    digits.remove(pos);
                }
            }
        }

        //return the rest
        digits
    }



    ///Read a puzzle from a file.
    ///
    ///The file must be formatted one row per line with values separated by commas with no trailing comma
    pub fn read_file<P: AsRef<Path>>(p: P) -> std::io::Result<Self> {
        let mut data: [Option<SudokuDigit>; 81] = [None; 81];
        let file = File::open(p)?;
        let reader = BufReader::new(file);
        for (y, line) in reader.lines().enumerate() {
            if y > 8 {
                panic!()
            }
            let line = line?;
            for (x, num) in line.split(",").map(|val| val.trim()).enumerate() {
                if x > 8 {
                    panic!()
                }
                data[x + y * 9] = match num {
                    "1" => Some(SudokuDigit::ONE),
                    "2" => Some(SudokuDigit::TWO),
                    "3" => Some(SudokuDigit::THREE),
                    "4" => Some(SudokuDigit::FOUR),
                    "5" => Some(SudokuDigit::FIVE),
                    "6" => Some(SudokuDigit::SIX),
                    "7" => Some(SudokuDigit::SEVEN),
                    "8" => Some(SudokuDigit::EIGHT),
                    "9" => Some(SudokuDigit::NINE),
                    "" => None,
                    _ => {
                        panic!()
                    }
                };
            }
        }
        Ok(Self { data })
    }


    ///Solves the puzzle recursively.
    ///
    ///The maximum recursive depth is 81.
    pub fn solve(&mut self) -> Result<(), SudokuError> {
        //attempt to find the most constrained empty square.
        //i.e. The empty square with the least possible valid digits.
        //The optimal amount of valid digits is one.
        if let Some(index) = self
            .get_empty_squares()
            .min_by_key(|val| self.get_valid_digits(*val).len())
        {
            let valid_digits = self.get_valid_digits(index);
            //If there is an empty square with no valid digits this puzzle is unsolvable
            if valid_digits.is_empty() {
                return Err(SudokuError::Unsolvable)
            }
            
            for digit in valid_digits {
                //attempt to insert each potential digit into the puzzle
                self.data[index.get_data_index()] = Some(digit);
                //attempt to solve the puzzle with that digit inserted
                if self.solve().is_ok() {
                    //Propagate the good news that the puzzle is solved up the stack.
                   return Ok(())
                }
            }
            //if none of the valid digits lead to a solved puzzle, this puzzle is unsolvable
            self.data[index.get_data_index()] = None;
            return Err(SudokuError::Unsolvable)
            
        } else {
            //If there are no empty digits, this puzzle is solved.
            //This is the base case.
            return Ok(())
        }
        
    }

    fn get_empty_squares(&self) -> SudokuEmptySquareIterator {
        SudokuEmptySquareIterator {
            x: 0,
            y: 0,
            puzzle: self,
        }
    }
}

impl Display for Sudoku {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        let mut out = String::new();
        for y in 0..9 {
            if y % 3 == 0 {
                if y == 0 {
                    out.push('┌');
                    for i in 0..11 {
                        if i == 3 || i == 7 {
                            out.push('┬');
                        } else {
                            out.push('─');
                        }
                    }
                    out.push('┐');
                } else {
                    out.push('├');
                    for i in 0..11 {
                        if i == 3 || i == 7 {
                            out.push('┼');
                        } else {
                            out.push('─');
                        }
                    }
                    out.push('┤');
                }
                out.push('\n');
            }
            for x in 0..9 {
                if x % 3 == 0 {
                    out.push('│');
                }
                if let Some(digit) = self.data[x + y * 9] {
                    out.push(match digit {
                        SudokuDigit::ONE => '1',
                        SudokuDigit::TWO => '2',
                        SudokuDigit::THREE => '3',
                        SudokuDigit::FOUR => '4',
                        SudokuDigit::FIVE => '5',
                        SudokuDigit::SIX => '6',
                        SudokuDigit::SEVEN => '7',
                        SudokuDigit::EIGHT => '8',
                        SudokuDigit::NINE => '9',
                    });
                } else {
                    out.push(' ');
                }
            }
            out.push('│');
            out.push('\n');
        }
        out.push('└');
        for i in 0..11 {
            if i == 3 || i == 7 {
                out.push('┴');
            } else {
                out.push('─');
            }
        }
        out.push('┘');
        out.push('\n');

        write!(f, "{}", out)
    }
}


#[cfg(test)]
mod tests {
    use crate::Sudoku;

    #[test]
    fn easy_solve() {
        let mut puzzle = Sudoku::read_file("puzzle.txt").unwrap();
        println!("{}", puzzle);
        puzzle.solve().unwrap();
        println!("{}", puzzle);
    }

    #[test]
    fn hard_solve() {
        let mut puzzle = Sudoku::read_file("puzzle2.txt").unwrap();
        println!("{}", puzzle);
        puzzle.solve().unwrap();
        println!("{}", puzzle);
    }
}